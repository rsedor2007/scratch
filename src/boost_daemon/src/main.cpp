// ----------------------------------------
// Boost
// ----------------------------------------
// #define BOOST_ASIO_ENABLE_OLD_SERVICES

#include <boost/asio.hpp>
#include <boost/array.hpp>
#include <boost/bind.hpp>
#include <boost/thread.hpp>

// ----------------------------------------
// std library
// ----------------------------------------
#include <array>
#include <string>
#include <ctime>
#include <iostream>
#include <cstdlib>
#include <utility>

// ----------------------------------------
// System
// ----------------------------------------
#include <syslog.h>
#include <unistd.h>

using boost::asio::ip::tcp;
using namespace std;

void thread_main(bool &running) {
    while (running) {
        //std::cout << "Thread running" << std::endl;
    }
}

int main() {

    try {

        boost::asio::io_service io_service;

        // Register the signal handlers
        boost::asio::signal_set signals(io_service, SIGINT, SIGTERM);
        signals.async_wait(boost::bind(&boost::asio::io_service::stop, &io_service));

        // Prepare the io_service to become a daemon
        io_service.notify_fork(boost::asio::io_service::fork_prepare);

        // Fork the process and exit the parent to become a daemon
        if (pid_t pid = fork()) {
            if (pid > 0) {
                syslog(LOG_ALERT | LOG_USER, "boost_daemon fork succeeded: %m");
                exit(0);
            }
            else {
                syslog(LOG_ERR | LOG_USER, "boost_daemon fork failed: %m");
                return 1;
            }
        }

        // Make the process a new session leader and detach from the terminal
        setsid();
        chdir("/");
        umask(0);

        if (pid_t pid = fork()) {
            if (pid > 0) {
                syslog(LOG_ALERT | LOG_USER, "Second fork succeeded: %m");
                exit(0);
            }
            else {
                syslog(LOG_ERR | LOG_USER, "Second fork failed: %m");
                return 1;
            }
        }

        // Decouple from the terminal
        close(0);
        close(1);
        close(2);

        if (open("/dev/null", O_RDONLY) < 0) {
            syslog(LOG_ERR | LOG_USER, "Unable to open /dev/null: %m");
            return 1;
        }

        syslog(LOG_ALERT | LOG_USER, "Opened /dev/null: %m");

        // Send standard output to a log file
        const char* output = "/tmp/boost_daemon.out";
        const int flags = O_WRONLY | O_CREAT | O_APPEND;
        const mode_t mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;
        if (open(output, flags, mode) < 0) {
            syslog(LOG_ERR | LOG_USER, "Unable to open output file %s: %m", output);
            return 1;
        }

        syslog(LOG_ALERT | LOG_USER, "Opened output file %s: %m", output);


        if (dup(1) < 0) {
            syslog(LOG_ERR | LOG_USER, "Unable to dup output descriptor: %m");
            return 1;
        }

        io_service.notify_fork(boost::asio::io_service::fork_child);

        syslog(LOG_INFO | LOG_USER, "boost_daemon started");

        // Initialize the threads here
        bool running = true;
        boost::thread_group threads;
        threads.create_thread(boost::bind(&thread_main, boost::ref(running)));

        io_service.run();

        running = false;
        threads.join_all();
        syslog(LOG_INFO | LOG_USER, "boost_daemon stopped");

    }
    catch (std::exception &e) {
        syslog(LOG_ERR | LOG_USER, "Exception: %s", e.what());
        std::cerr << "Exception: " << e.what() << std::endl;
    }
}
