
// ----------------------------------------
// Boost
// ----------------------------------------
// #define BOOST_ASIO_ENABLE_OLD_SERVICES

#include <boost/asio.hpp>
#include <boost/asio/ip/tcp.hpp>

// ----------------------------------------
// std library
// ----------------------------------------
#include <array>
#include <vector>
#include <functional>
#include <string>
#include <ctime>
#include <iostream>
#include <cstdlib>
#include <thread>
#include <utility>

// ----------------------------------------
// System
// ----------------------------------------
#include <syslog.h>
#include <unistd.h>

#include "addressbook.pb.h"
#include "test.h"

using boost::asio::ip::tcp;
using namespace std;

const int max_length = 1024;

void noop() {}

enum
{
    header_size = sizeof(std::size_t)
};

std::vector<char> build_header(const std::string& body)
{
    std::vector<char> buffer(header_size);
    auto body_size = body.size();
    std::memcpy(&buffer[0], &body_size, sizeof body_size);
    return buffer;
}

std::size_t parse_header(const std::vector<char>& buffer)
{
    return *reinterpret_cast<const std::size_t*>(&buffer[0]);
}

void session(tcp::socket sock)
{
    try
    {
        for (;;)
        {
            char data[max_length];

            boost::system::error_code error;
            size_t length = sock.read_some(boost::asio::buffer(data), error);

            if (error == boost::asio::error::eof)
                break; // Connection closed
            else if (error)
                throw boost::system::system_error(error); // Throw some error

            string reply(data);
            // reply.reserve(max_length);

            address::AddressBook addressBookOut;
            addressBookOut.ParseFromString(reply);

            cout << "Client Input Phone Number: " << addressBookOut.person(0).phone(0).number() << endl;

            string out("xxx.xxx.xxxx");
            addressBookOut.mutable_person(0)->mutable_phone(0)->set_number(out);

            // reply.append(" Earth Here!");
            string message;
            message.reserve(max_length);
            addressBookOut.SerializeToString(&message);

            boost::asio::write(sock, boost::asio::buffer(message.c_str(), max_length));
        }

    }
    catch(exception& e)
    {
        cerr << "Exception in the session: " << e.what() << endl;
    }

}

void dynSession(tcp::socket sock)
{
    try
    {
        for (;;)
        {
            // char data[max_length];

            std::vector<char> buffer;

            // Read header
            buffer.resize(header_size);
            boost::asio::read(sock, boost::asio::buffer(buffer));

            cout << "Size of the header: " << &buffer[0] << endl;

            // Extract body size from header, resize buffer, then read body
            auto body_size = parse_header(buffer);
            buffer.resize(body_size);

            boost::system::error_code error;
            boost::asio::read(sock, boost::asio::buffer(buffer));

            string reply(buffer.begin(), buffer.end());

            address::AddressBook addressBookOut;
            addressBookOut.ParseFromString(reply);

            cout << "Client Input Phone Number: " << addressBookOut.person(0).phone(0).number() << endl;

            string out("xxx.xxx.xxxx");
            addressBookOut.mutable_person(0)->mutable_phone(0)->set_number(out);

            string message;
            addressBookOut.SerializeToString(&message);

            auto header = build_header(message);

            // Gather header and body into a single buffer
            std::array<boost::asio::const_buffer, 2> buffers = {{
                boost::asio::buffer(header),
                boost::asio::buffer(message)
            }};
            boost::asio::write(sock, buffers);
        }

    }
    catch(exception& e)
    {
        cerr << "Exception in the session: " << e.what() << endl;
    }

}

void server(boost::asio::io_service& io_service, unsigned short port)
{
    // endpoint ep(boost::asio::ip::address::from_string("0.0.0.1"), 1001);
    tcp::acceptor a(io_service, tcp::endpoint(tcp::v4(), port));
    for (;;)
    {
        tcp::socket sock(io_service);

        // synchronous read socket
         a.accept(sock);

        // dynamic read socket
//        a.async_accept(sock, std::bind(&noop));

//        io_service.run();
//        io_service.reset();

        // synchronous read session
//         thread(session, std::move(sock)).detach();

        // dynamic read session
        thread(dynSession, std::move(sock)).detach();
    }
}

int main()
{
    GOOGLE_PROTOBUF_VERIFY_VERSION;

    try
    {
        boost::asio::io_service io_service;
        server(io_service, 8081);

    }
    catch(exception& e)
    {
        cerr << "Exception: " << e.what() << endl;

    }

    // Optional:  Delete all global objects allocated by libprotobuf.
    google::protobuf::ShutdownProtobufLibrary();

    return 0;

}

