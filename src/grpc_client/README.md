# GRPC and Protobuf POC

- [Overview](#Overview)
- [Installation](#Installation)

# Overview

This page gives instructions on adding both GRPC and Protobuf to the project using CMake

## Reading Material

Where is a good place to start on GRPC and Protobuf?

[Chromium Examples](https://chromium.googlesource.com/external/github.com/grpc/grpc/+/chromium-deps/2016-08-17/examples)

[GRPC Examples](https://github.com/grpc/grpc/tree/master/examples)

[Third Party Example](https://github.com/jan-alexander/grpc-cpp-helloworld-cmake) - Jan Alexander

[Third Party Example](https://github.com/IvanSafonov/grpc-cmake-example) - Ivan Safonov

[Medium Article](https://medium.com/@andrewvetovitz/grpc-c-introduction-45a66ca9461f)

# Installation

When installing the General RPC framework and Protobuf, our targets will be AWS Linux (Centos) or Debian based Ubuntu.

[Ubuntu GRPC and Protobuf](https://packages.ubuntu.com/source/bionic/grpc) packages

The better approach is to download the project from Git, build it and then deploy it on the Linux distro.

[Official Google](https://github.com/grpc/grpc/blob/v1.18.0/src/cpp/README.md) CMake instructions.

[Protobuf Install](https://github.com/protocolbuffers/protobuf/blob/master/src/README.md)

Compiling and running the application as part of your CMake application will pull in all of GRPC and Protobuf and their
dependencies as part of the project.  This is not a recommended approach but, will get the examples up and running quickly
to prototype with.

Under the GRPC directory is a test directory with a bash script you can use to model this type of installation:

````
grpc/test/distrib/cpp/run_distrib_test_cmake.sh
````
## GRPC and Protobuf Installation Instructions

The following instructions work on Linux.  GRPC download now includes Protobuf.

Before you start, go here:

[Install](https://grpc.io/blog/installation) by language.

### Clone the repo

Clone the repo into the directory you want to build from:

[GRPC](https://github.com/grpc/grpc) github repo

### Install the prerequisites (no CMake)

The following instructions will allow you to build without CMake on the target.

Ubuntu docker file instructions.  These same dependencies and instructions can be used on Ubuntu outside of docker.
````
# Ubuntu docker file
FROM ubuntu:18.04

# Update environment and install dependencies
RUN apt-get update && \
    apt-get -y install \
  	build-essential \
	autoconf \
	libtool \
	libgflags-dev \
	libgtest-dev \
	clang \
	libc++-dev \
	curl \
	git && \
    apt-get clean

# Clone latest GRPC, build and install  
# If you give a version number, it will clone that version
# If you do not specify a version number, it will clone from the master
RUN cd /usr/local/src && \
    git clone -b v1.4.2 https://github.com/grpc/grpc && \
    cd grpc && \
    git submodule update --init --recursive && \
    make -j 8 && \
    make install
````
The following docker file can be used on Centos 7:
````
FROM centos:7

# Update environment and install dependencies
RUN yum update -y && \
    yum install -y \
  	build-essential \
	autoconf \
	libtool \
	gflags-develop \
	gtest-develop \
	clang \
	libstdc++-devel \
	curl \
	git \
	make \ 
	gcc \
	gcc-c++ \
	which && \
    yum clean all
  
# Clone latest GRPC, build and install  
# If you give a version number, it will clone that version
# If you do not specify a version number, it will clone from the master  
RUN cd /usr/local/src && \
    git clone -b v1.4.2 https://github.com/grpc/grpc && \
    cd grpc && \
    git submodule update --init --recursive && \
    make -j 8 && \
    make install
````
### Install with CMake

On your development or build machine, follow the previous instructions calling CMake prior to make:

* Install GRPC and Protobuf

````
git clone -b $(curl -L https://grpc.io/release) https://github.com/grpc/grpc
cd grpc
git submodule update --init
 
# Build and install protobuf
cd ./third_party/protobuf
./autogen.sh
./configure --prefix=/opt/protobuf
make -j `nproc`
sudo make install
  
# Build and install gRPC
cd ../..
make -j `nproc` PROTOC=/opt/protobuf/bin/protoc 
sudo make prefix=/opt/grpc install
````

*  Make a build directory inside of GRPC directory and run CMake
````
mkdir build
cd build
cmake ..
make -j `nproc`
make check
sudo make install
````
* Set the LD_LIBRARY_PATH cache update
You could either set the path in your profile config file **.bashrc** or **.bash_profile** or you can call **ldconfig**.

````
# Either run ldconfig to update the cache or set your LD_LIBRARY_PATH
sudo ldconfig # refresh the shared library cache
````

[Man Pages](https://linux.die.net/man/8/ldconfig) for **ldconfig**

* Reset the configure path

If you want it to be installed in **/usr/local** or **/usr/local/lib** reset and invoke configure again.

````
./configure --prefix=/usr
````

**/usr/local/lib** may not be part of **LD_LIBRARY_PATH** depending on the distro you are on.

Make sure to do a **make clean** prior to building again.