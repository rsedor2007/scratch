
// -----------------------------------------------------
// Boost
// -----------------------------------------------------
#include <boost/thread/mutex.hpp>
#include <boost/thread/locks.hpp>
#include <boost/thread/condition_variable.hpp>

// -----------------------------------------------------
#include "work_queue.h"

void work_queue::push_task(const task_type &task) {
    boost::unique_lock <boost::mutex> lock(tasks_mutex_);

    tasks_.push_back(task);

    lock.unlock();

    cond_.notify_one();
}

work_queue::task_type work_queue::try_pop_task() {
    work_queue::task_type ret;

    boost::lock_guard <boost::mutex> lock(tasks_mutex_);
    if (!tasks_.empty()) {
        ret = tasks_.front();
        tasks_.pop_front();
    }

    return ret;
}

work_queue::task_type work_queue::pop_task() {
    boost::unique_lock <boost::mutex> lock(tasks_mutex_);

    while (tasks_.empty()) {
        cond_.wait(lock);
    }

    task_type ret = tasks_.front();
    tasks_.pop_front();

    return ret;
}

void work_queue::flush() {
    boost::lock_guard<boost::mutex> lock(tasks_mutex_);
    cond_.notify_all();
}
