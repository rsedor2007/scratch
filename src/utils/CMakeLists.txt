cmake_minimum_required(VERSION 3.12)
project(utils)

add_subdirectory(tcpconfig)
add_subdirectory(tcpclient)
add_subdirectory(work_queue)