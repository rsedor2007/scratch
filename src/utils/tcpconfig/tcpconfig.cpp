
#include <iostream>
#include "tcpconfig.h"

namespace utils {
    namespace tcp_utils {

        TcpConfig::TcpConfig(const std::string& configPath)
        {
            load (configPath);
        }

        std::string TcpConfig::getString(const std::string &key) const
        {
            try
            {
                if (boost::optional<std::string> str = json.get_optional<std::string>(key))
                {
                    std::cout << std::boolalpha << str.is_initialized() << std::endl;
                    if (!str.is_initialized())
                        throw std::invalid_argument("TcpConfig string key not found: " + key);

                    return *str;
                }
            }
            catch (std::invalid_argument& e)
            {
                throw std::runtime_error(e.what());
            }
        }

        int TcpConfig::getInt(const std::string &key) const
        {
            try
            {
                boost::optional<int> i = json.get_optional<int>(key);

                if ( !i.is_initialized() )
                    throw std::invalid_argument("TcpConfig int key not found: " + key);

                return *i;
            }
            catch (std::invalid_argument& e)
            {
                throw std::runtime_error(e.what());
            }

        }

        std::vector<std::string> TcpConfig::getStringArray(const std::string& key) const
        {
            std::vector<std::string> array;

            for(auto& node : json.get_child(key))
            {
                assert(node.first.empty());
                array.push_back(node.second.data());
            }
            return array;
        }

        std::vector<std::pair<std::string, std::string>> TcpConfig::getListPair(const std::string& key) const
        {
            std::vector<std::pair<std::string, std::string>> matrix;

            for (auto& node : json.get_child(key))
            {
                std::string name(node.first);
                std::string value(node.second.data());
                matrix.push_back(std::make_pair(name, value));
            }

            return matrix;
        }

        void TcpConfig::load(const std::string &configPath)
        {
            boost::property_tree::json_parser::read_json(configPath, json);
        }
    }
}