
#include <cstdlib>
#include <cstring>
#include <string>
#include <iostream>

#include "tcpclient.h"

using namespace std;

namespace utils {
    namespace tcp_utils {
        SyncTcpClient::SyncTcpClient(boost::asio::io_service &ios, boost::asio::ip::tcp::resolver::iterator endpoint_iterator) : _ios(ios), sock(ios) {
            connect(endpoint_iterator);
        }

        SyncTcpClient::~SyncTcpClient() {
            close();
        }

        void SyncTcpClient::connect(boost::asio::ip::tcp::resolver::iterator endpoint_iterator) {


/*
            tcp::resolver::query query(raw_ip_address, port);
            if (not_throw) {
                std::stringstream msg;
                msg << "Error resolving host(" << not_throw.value() << "): " << not_throw.message() << std::endl;
                throw std::invalid_argument(msg.str());
            }
*/
            boost::asio::connect(sock, endpoint_iterator, not_throw);
            if (not_throw) {
                std::stringstream msg;
                msg << "Error connecting(" << not_throw.value() << "): " << not_throw.message() << std::endl;
                throw std::runtime_error(msg.str());
            }
/*
            boost::asio::connect(sock, resolver.resolve(query, not_throw));
            if (not_throw) {
                std::stringstream msg;
                msg << "Error connecting(" << not_throw.value() << "): " << not_throw.message() << std::endl;
                throw std::runtime_error(msg.str());
            }
*/

        }

        void SyncTcpClient::close() {
            sock.shutdown(boost::asio::ip::tcp::socket::shutdown_both);
            sock.close();
        }

//        void SyncTcpClient::sendMessage(const std::string &message) {
//            std::size_t size = boost::asio::write(sock, boost::asio::buffer(message.c_str(), message.length()));
//            if (not_throw) {
//                std::stringstream msg;
//                msg << "Error sending(" << not_throw.value() << "): " << not_throw.message() << std::endl;
//                throw std::runtime_error(msg.str());
//            }
//        }
//
//        std::string SyncTcpClient::readResponse() {
//            char request[max_length];
//            size_t reply_length = boost::asio::read(sock, boost::asio::buffer(request, max_length), not_throw);
//            if (not_throw) {
//                std::stringstream msg;
//                msg << "Error reading(" << not_throw.value() << "): " << not_throw.message() << std::endl;
//                throw std::runtime_error(msg.str());
//            }
//
//            std::string reply(request);
//            return reply;
//        }
        void SyncTcpClient::sendMessage(const std::string &message) {

            // Build the header
            auto header = buildHeader(message);

            // Gather header and body into single buffer
            std::array<boost::asio::const_buffer, 2> buffers = {{
                                                                        boost::asio::buffer(header),
                                                                        boost::asio::buffer(message)
                                                                }};

            std::size_t size = boost::asio::write(sock, buffers, not_throw);
            if (not_throw) {
                std::stringstream msg;
                msg << "Error sending(" << not_throw.value() << "): " << not_throw.message() << std::endl;
                throw std::runtime_error(msg.str());
            }
        }

        std::string SyncTcpClient::readResponse() {

            std::vector<char> buffer;

            // Read header
            buffer.resize(this->header_size);
            boost::asio::read(sock, boost::asio::buffer(buffer));

            // Extract body size from header, resize buffer, then read body
            auto body_size = parseHeader(buffer);
            buffer.resize(body_size);
            boost::asio::read(sock, boost::asio::buffer(buffer), not_throw);
            if (not_throw) {
                std::stringstream msg;
                msg << "Error reading(" << not_throw.value() << "): " << not_throw.message() << std::endl;
                throw std::runtime_error(msg.str());
            }

            std::string reply(buffer.begin(), buffer.end());
            return reply;
        }

        std::vector<char> SyncTcpClient::buildHeader(const std::string &body) {
            std::vector<char> buffer(this->header_size);
            auto body_size = body.size();
            std::memcpy(&buffer[0], &body_size, sizeof body_size);
            return buffer;
        }

        std::size_t SyncTcpClient::parseHeader(const std::vector<char> &buffer) {
            return *reinterpret_cast<const std::size_t*>(&buffer[0]);
        }

    }
}


