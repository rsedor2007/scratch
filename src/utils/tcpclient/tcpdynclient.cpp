
//#include <cstdlib>
//#include <cstring>
//#include <string>
//#include <iostream>

#include "tcpdynclient.h"

using namespace std;

namespace utils {
    namespace tcp_utils {

        void noop() {}

        TcpDynClient::TcpDynClient(boost::asio::io_service &ios, boost::asio::ip::tcp::acceptor &acceptor) : _ios(ios), sock(ios) {
            connect(acceptor);
        }

        TcpDynClient::~TcpDynClient() {
            close();
        }

        void TcpDynClient::connect(boost::asio::ip::tcp::acceptor &acceptor) {

            try {
                boost::system::error_code error;
//                sock.async_connect(acceptor.local_endpoint(), std::bind(&noop));
                sock.async_connect(acceptor.local_endpoint(), std::bind(&noop));
                _ios.run();
//                _ios.reset();
            }
            catch(std::exception& e) {
                std::stringstream msg;
                msg << "Error connecting(" << e.what() << ")" << std::endl;
//                throw std::runtime_error(msg.str());
            }
        }

        void TcpDynClient::close() {
            sock.shutdown(boost::asio::ip::tcp::socket::shutdown_both);
            sock.close();
        }

        void TcpDynClient::sendMessage(const std::string &message) {

            // Build the header
            auto header = buildHeader(message);

            // Gather header and body into single buffer
            std::array<boost::asio::const_buffer, 2> buffers = {{
                boost::asio::buffer(header),
                boost::asio::buffer(message)
            }};

            std::size_t size = boost::asio::write(sock, buffers, not_throw);
            if (not_throw) {
                std::stringstream msg;
                msg << "Error sending(" << not_throw.value() << "): " << not_throw.message() << std::endl;
                throw std::runtime_error(msg.str());
            }
        }

        std::string TcpDynClient::readResponse() {

            std::vector<char> buffer;

            // Read header
            buffer.resize(this->header_size);
            boost::asio::read(sock, boost::asio::buffer(buffer));

            // Extract body size from header, resize buffer, then read body
            auto body_size = parseHeader(buffer);
            buffer.resize(body_size);
            boost::asio::read(sock, boost::asio::buffer(buffer), not_throw);
            if (not_throw) {
                std::stringstream msg;
                msg << "Error reading(" << not_throw.value() << "): " << not_throw.message() << std::endl;
                throw std::runtime_error(msg.str());
            }

            std::string reply(buffer.begin(), buffer.end());
            return reply;
        }

        std::vector<char> TcpDynClient::buildHeader(const std::string &body) {
            std::vector<char> buffer(this->header_size);
            auto body_size = body.size();
            std::memcpy(&buffer[0], &body_size, sizeof body_size);
            return buffer;
        }

        std::size_t TcpDynClient::parseHeader(const std::vector<char> &buffer) {
            return *reinterpret_cast<const std::size_t*>(&buffer[0]);
        }

    }
}


