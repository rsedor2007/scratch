#pragma once
#ifndef __EVALUATE_WORKQ_H__
#define __EVALUATE_WORKQ_H__

#include "gtest/gtest.h"

// ---------------------------------------------------------
// Standard Library
// ---------------------------------------------------------
#include <iostream>
#include <string>

// ---------------------------------------------------------
// Boost
// ---------------------------------------------------------
#include <boost/thread/thread.hpp>

// ---------------------------------------------------------
#include "work_queue.h"

namespace {

    class TestQueue {
    public:
        work_queue g_queue;

        static void some_task() {
            std::cout << "task" << std::endl;
        }

        const std::size_t tests_tasks_count = 10 /*000*/;

        void pusher() {
            for (std::size_t ndx = 0; ndx < tests_tasks_count; ++ndx) {
                this->g_queue.push_task(&TestQueue::some_task);
            }
        }

        void popper_sync() {
            for (std::size_t ndx = 0; ndx < tests_tasks_count; ++ndx) {
                work_queue::task_type t = this->g_queue.pop_task();

                t();
            }
        }

    };

    class Evaluate : public testing::Test {
    };

    TEST_F(Evaluate, Evaluate_Multithread_Test) {

        TestQueue testQueue;
        boost::thread pop_sync1(boost::bind(&TestQueue::popper_sync, &testQueue));
        boost::thread pop_sync2(boost::bind(&TestQueue::popper_sync, &testQueue));

        boost::thread push1(boost::bind(&TestQueue::pusher, &testQueue));
        boost::thread push2(boost::bind(&TestQueue::pusher, &testQueue));

        push1.join();
        push2.join();
        testQueue.g_queue.flush();

        pop_sync1.join();
        pop_sync2.join();

        ASSERT_TRUE(!testQueue.g_queue.try_pop_task());

        testQueue.g_queue.push_task(&TestQueue::some_task);

        ASSERT_TRUE(testQueue.g_queue.try_pop_task());
    }

}

#endif //__EVALUATE_WORKQ_H__
