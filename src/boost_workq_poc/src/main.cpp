
// ---------------------------------------------------------
// Standard Library
// ---------------------------------------------------------
#include <iostream>
#include <string>

#include "evaluate_workq.h"

int main(int ac, char* av[])
{
    testing::InitGoogleTest(&ac, av);
    return RUN_ALL_TESTS();
}
