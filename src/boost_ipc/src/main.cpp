
// ---------------------------------------------------------
// Boost
// ---------------------------------------------------------
#include <boost/interprocess/ipc/message_queue.hpp>

// ---------------------------------------------------------
// Standard Library
// ---------------------------------------------------------
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>

#include "vrmessage.pb.h"
#include "tank.pb.h"

#include "sharedmq.h"

using namespace boost::interprocess;

const int max_length = 1024;

std::string addTank() {

    vrmessage::Envelope env;
    vrmessage::Message *msg = env.mutable_message();
    vrmessage::Message_Header *header = msg->mutable_header();

    header->set_messageid("test");
    header->set_type("bad test");


    tank::Tank *tank = msg->MutableExtension(tank::Tank::tank);

    tank::Tank_request_tank_setup *tankRequest = tank->mutable_tanksetup();

    tankRequest->set_messageid(10);
    tankRequest->set_tankid(11);

    std::string message;
    message.reserve(max_length);

    env.SerializeToString(&message);

    return message;
}

int main() {

    GOOGLE_PROTOBUF_VERIFY_VERSION;

    try {

//        message_queue::remove("message_queue");
//
//        message_queue mq
//                (create_only,
//                 "message_queue",
//                 100,
//                 sizeof(char)* max_length);

        SharedMQ sharedMQ("message_queue", 100);
        std::string msg = addTank();
        sharedMQ.sendMessage(msg);

//        mq.send(msg.data(), sizeof(msg), 0);

    }
    catch (interprocess_exception &e) {
        std::cout << e.what() << std::endl;
        return 1;
    }
    catch (std::exception &e) {
        std::cout << e.what() << std::endl;
        return 1;
    }

    google::protobuf::ShutdownProtobufLibrary();

    return 0;
}
