#pragma once
#ifndef __SHAREDMQ_H__
#define __SHAREDMQ_H__


#include <boost/interprocess/ipc/message_queue.hpp>

using namespace boost::interprocess;

class SharedMQ {
public:
    SharedMQ(const std::string &name, const unsigned int max_queue_size) :
        SharedMQ{name, max_queue_size, deleteMQ(name)}
    {}

    explicit SharedMQ(const std::string &name) : _mq{ boost::interprocess::open_only, name.c_str() }
    {}

    ~SharedMQ() {
        deleteMQ(_name);
    }

    void sendMessage(const std::string &msg) {
        sendMessage(msg, 0);
    }

    void sendMessage(const std::string &msg, unsigned int priority) {
        _mq.send(msg.data(), msg.length(), priority);
    }

    std::string getMsg() {
        unsigned int priority;
        message_queue::size_type recvd_size;

        char received[BUFFER_SIZE];
        _mq.receive(received, sizeof(char) * BUFFER_SIZE, recvd_size, priority);

        if (recvd_size > BUFFER_SIZE)
            throw interprocess_exception("message was too large");

        std::string msg(received);

        return msg;
    }

private:
    std::string _name;

    const int BUFFER_SIZE = 1024;

    SharedMQ(const std::string &name, const unsigned int max_queue_size, bool) :
            _mq{boost::interprocess::create_only, name.c_str(), max_queue_size, sizeof(char) * BUFFER_SIZE}
    {}

    bool deleteMQ (const std::string &name) {
        return boost::interprocess::message_queue::remove(name.c_str());
    }

    boost::interprocess::message_queue _mq;
};

#endif //__SHAREDMQ_H__
