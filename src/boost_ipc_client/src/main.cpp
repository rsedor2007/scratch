
// ---------------------------------------------------------
// Boost
// ---------------------------------------------------------
#include <boost/interprocess/ipc/message_queue.hpp>

// ---------------------------------------------------------
// Standard Library
// ---------------------------------------------------------
#include <iostream>
#include <vector>
#include <string>


#include "vrmessage.pb.h"
#include "tank.pb.h"

using namespace boost::interprocess;

const int max_length = 1024;

void writeMessage(const std::string &sent) {

    vrmessage::Envelope env;
    env.ParseFromString(sent);



    vrmessage::Message *msg = env.mutable_message();
    vrmessage::Message_Header *header = msg->mutable_header();

    std::cout << "Message Id: " << header->messageid() << std::endl;
    std::cout << "Message Type: " << header->type() << std::endl;

    tank::Tank *tank = msg->MutableExtension(tank::Tank::tank);

    tank::Tank_request_tank_setup *tankRequest = tank->mutable_tanksetup();

    std::cout << "Tank Request Message Id: " << tankRequest->messageid() << std::endl;
    std::cout << "Tank Request Tank Id: " << tankRequest->tankid() << std::endl;

    return;
}

int main ()
{
    GOOGLE_PROTOBUF_VERIFY_VERSION;

    try{
        //Open a message queue.
        message_queue mq
                (open_only        //only create
                        ,"message_queue"  //name
                );

        unsigned int priority;
        message_queue::size_type recvd_size;

        char received[max_length];
        mq.receive(received, sizeof(char) * max_length, recvd_size, priority);

        std::string msg(received);

        if (recvd_size > max_length)
            throw interprocess_exception("message was too large");

        writeMessage(msg);

    }
    catch(interprocess_exception &ex){
        message_queue::remove("message_queue");
        std::cout << ex.what() << std::endl;
        return 1;
    }
    message_queue::remove("message_queue");


    google::protobuf::ShutdownProtobufLibrary();

    return 0;
}
