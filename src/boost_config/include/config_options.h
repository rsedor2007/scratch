
#pragma  once
#ifndef __CONFIG_OPTIONS_H__
#define __CONFIG_OPTIONS_H__


// ---------------------------------------------------------------------------
// Boost
// ---------------------------------------------------------------------------
#include <boost/program_options.hpp>

// ---------------------------------------------------------------------------
// Standard Library
// ---------------------------------------------------------------------------
#include <iostream>
#include <vector>
#include <fstream>
#include <iterator>

using namespace std;
using namespace boost::program_options;

template <class T>
ostream& operator<<(ostream& os, const vector<T>& v) {
    copy(v.begin(), v.end(), ostream_iterator<T>(cout, " "));
    return os;
}

class ConfigOptions
{
public:
    ConfigOptions (int ac, char* av[]) {
        initOptions(ac, av);
    }

    string getStringVariable(const string &name){
        if (_vm.count(name)) {
            return *_vm[name].as<string_vector>().data();
        }
    }

    string handleCmdLine() {
        if (_vm.count("help")) {
            cout << _cmdlineOptions << endl;
            return string{"help"};  // TODO: Cause this to do something
        }

        return string{"unknown"};
    }

private:
    typedef vector<string> string_vector;
    options_description _cmdlineOptions;
    options_description _configfileOptions;
    variables_map _vm;

    void initOptions(int ac, char* av[]) {

        // Build the generic command-line options
        options_description generic("Generic Options");
        cmdlineOptions(generic);

        // Build the config-file options

        // Build group options for both the config-file and command-line
        boost::program_options::options_description config("Configuration");
        groupOptions(config);

        // Compose the options
        _cmdlineOptions.add(generic).add(config);
        _configfileOptions.add(config);

        // Compose the variables
        store(boost::program_options::parse_command_line(ac, av, _cmdlineOptions), _vm);

        // Compose the config-file
        ifstream ifs("../conf/boost_config.cfg");
        store(parse_config_file(ifs, _configfileOptions), _vm);
        notify(_vm);

    }

    void cmdlineOptions(options_description &generic) {
        generic.add_options()
                ("version,v", "print version string")
                ("help,h", "produce help message");
    }

    void configFileOptions() {
    }

    void groupOptions(options_description &config) {
        config.add_options()
                ("queue-name,Q", boost::program_options::value<string_vector>()->composing(), "queue name");
    }
};
#endif //__CONFIG_OPTIONS_H__
