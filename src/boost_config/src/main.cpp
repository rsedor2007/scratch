
// ---------------------------------------------------------------------------
// Boost
// ---------------------------------------------------------------------------
#include <boost/program_options.hpp>

// ---------------------------------------------------------------------------
// Standard Library
// ---------------------------------------------------------------------------
#include <iostream>
#include <vector>
#include <fstream>
#include <iterator>

#include "config_options.h"

using namespace std;


int main(int ac, char* av[]) {

    ConfigOptions configOptions(ac, av);

    string queueName{configOptions.getStringVariable("queue-name")};

    cout << "Queue Name: " << queueName << endl;

    string help{configOptions.handleCmdLine()};

    cout << "Help: " << help << endl;

    return 0;
}