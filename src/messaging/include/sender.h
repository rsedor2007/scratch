
#pragma  once
#ifndef __SENDER_H__
#define __SENDER_H__

#include "message.h"

namespace messaging {
    class sender {
        queue *q;

    public:
        sender() : q(nullptr) {}
        explicit sender(queue *q_) : q(q_) {}

        template <typename Message>
        void send(Message const& msg) {
            if (q) {
                q->push(msg);
            }
        }
    };
}

#endif //__SENDER_H__
