
#pragma once
#ifndef __MESSAGE_H__
#define __MESSAGE_H__

#include <mutex>
#include <condition_variable>
#include <queue>
#include <memory>

namespace messaging {

    struct message_base {
        virtual ~message_base(){}
    };

    template<typename msg>
    struct wrapped_message : message_base {
        msg contents;

        explicit wrapped_message(msg const& contents_) : contents(contents_) {}
    };

    class queue {
        std::mutex m;
        std::condition_variable c;
        std::queue<std::shared_ptr<message_base>> q;

    public:
        template<typename T>
        void push(T const& msg) {
            std::lock_guard<std::mutex> lk(m);
            q.push(std::make_shared<wrapped_message<T>>(msg));
            c.notify_all();
        }

        std::shared_ptr<message_base> wait_and_pop() {
            std::unique_lock<std::mutex> lk(m);
            c.wait(lk, [&]{return !q.empty();});
            auto res = q.front();
            q.pop();
            return res;
        }
    };
}

#endif //__MESSAGE_H__
