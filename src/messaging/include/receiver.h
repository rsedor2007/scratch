
#pragma once
#ifndef __RECEIVER_H__
#define __RECEIVER_H__

class queue;
class sender;
#include "dispatcher.h"

namespace messaging {
    class receiver {
        queue q;

    public:
        operator sender() {
            return sender(&q);
        }

        dispatcher wait() {
            return dispatcher(&q);
        }
    };
}

#endif //__RECEIVER_H__
