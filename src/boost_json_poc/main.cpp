#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
//#include <boost/foreach.hpp>
#include <cassert>
#include <exception>
#include <iostream>
#include <string>

#include "utils/tcpconfig/tcpconfig.h"

using namespace utils::tcp_utils;

int main()
{
    try
    {
        std::string configFile = "../conf/boost_json_poc.cfg";

        TcpConfig config(configFile);

        // Get an integer
        auto name = config.getInt("height");

        std::cout << "Value of height: " << name << std::endl;

        // Get some complex string
        auto complex = config.getString("some.complex.path");

        std::cout << "Value of some.complex.path: " << complex << std::endl;

        // Get some array
        auto fruits = config.getStringArray("fruits");

        for (auto& node : fruits)
        {
            std::cout << node << std::endl;
        }

        // Get some array
        auto animals = config.getListPair("animals");

        for (auto& node : animals)
        {
            std::cout << "<name>" << node.first << "</name><value>" << node.second << "</value>" << std::endl;
        }

    }
    catch (std::exception const& e)
    {
        std::cerr << e.what() << std::endl;
    }

    return 0;
}
