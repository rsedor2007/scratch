
// #define BOOST_ASIO_DISABLE_KQUEUE 1
// #include <deque>

#include <cstdlib>
#include <cstring>
#include <string>
#include <iostream>

#include <thread>
#include <boost/asio.hpp>

#include "tcpclient.h"
//#include "tcpdynclient.h"

#include "addressbook.pb.h"

using boost::asio::ip::tcp;

using namespace std;

enum { max_length = 1024 };

void addAddress(address::AddressBook& addressBook, const std::string &number)
{
    address::Person *person = addressBook.add_person();
    person->set_id(124);
    person->set_name("Julio");
    address::Person_PhoneNumber *phoneNumber= person->add_phone();
    phoneNumber->set_number(number);

    return;
}

int main(int argc, char** argv)
{
    GOOGLE_PROTOBUF_VERIFY_VERSION;

    try
    {
        boost::asio::io_service ios;
//        boost::asio::ip::tcp::acceptor acceptor(ios, tcp::endpoint(tcp::v4(), 8081));
//        acceptor.set_option(tcp::acceptor::reuse_address(true));
//        utils::tcp_utils::TcpDynClient tcpDynClient(ios, acceptor);

        boost::asio::ip::tcp::resolver resolver(ios);
        auto endpoint_iterator = resolver.resolve({"localhost", "8081"});
        utils::tcp_utils::SyncTcpClient tcpClient(ios, endpoint_iterator);

        std::thread t([&ios](){ ios.run(); });

        char line[1024];
        while (std::cin.getline(line, 1024)) {
            address::AddressBook addressBook;

            std::string number(line);
            addAddress(addressBook, number);

            string message;
            message.reserve(max_length);

            addressBook.SerializeToString(&message);

            tcpClient.sendMessage(message);
//            tcpDynClient.sendMessage(message);

            string reply = tcpClient.readResponse();
//            string reply = tcpDynClient.readResponse();

            address::AddressBook addressBookOut;
            addressBookOut.ParseFromString(reply);

            cout << "Reply is: " << addressBookOut.person(0).phone(0).number() << endl;

        }

        t.join();

    }
    catch (exception& e)
    {
        cerr << "Exception: " << e.what() << endl;
    }

    // Optional:  Delete all global objects allocated by libprotobuf.
    google::protobuf::ShutdownProtobufLibrary();

    return 0;
}


