#pragma once
#ifndef __WORK_QUEUE_H__
#define __WORK_QUEUE_H__

// -----------------------------------------------------
// Standard Library
// -----------------------------------------------------
#include <deque>

// -----------------------------------------------------
// Boost
// -----------------------------------------------------
#include <boost/function.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/locks.hpp>
#include <boost/thread/condition_variable.hpp>

class work_queue {

public:
    typedef boost::function<void()> task_type;

private:
    std::deque<task_type> tasks_;
    boost::mutex tasks_mutex_;
    boost::condition_variable cond_;

public:
    void push_task(const task_type &task);
    task_type try_pop_task();
    task_type pop_task();
    void flush();
};


#endif //__WORK_QUEUE_H__
