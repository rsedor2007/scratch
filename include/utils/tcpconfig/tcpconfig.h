
#pragma once
#ifndef TCPCONFIG_H
#define TCPCONFIG_H

#include <vector>
#include <string>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

namespace utils {
    namespace tcp_utils {
        class TcpConfig {
        public:
            explicit TcpConfig (const std::string& configPath);

            TcpConfig() = delete;

            std::string getString (const std::string& key) const;

            int getInt (const std::string& key) const;

            std::vector<std::string> getStringArray(const std::string& key) const;

            std::vector<std::pair<std::string, std::string>> getListPair(const std::string& key) const;

        private:
            void load (const std::string& configPath);

        private:
            boost::property_tree::ptree json;

        };
    }
}

#endif //TCPCONFIG_H
