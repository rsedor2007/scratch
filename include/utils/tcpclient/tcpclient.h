#pragma once
#ifndef __TCPCLIENT_H__
#define __TCPCLIENT_H__

// ------------------------------------
// Standard Library
// ------------------------------------
#include <string>

// ------------------------------------
// Boost
// ------------------------------------
#include <boost/asio.hpp>

using boost::asio::ip::tcp;

namespace utils {
    namespace tcp_utils {
        class SyncTcpClient {
        public:
            explicit SyncTcpClient(boost::asio::io_service &ios, boost::asio::ip::tcp::resolver::iterator endpoint_iterator);

            SyncTcpClient() = delete;

            ~SyncTcpClient();

        private:
            void connect(boost::asio::ip::tcp::resolver::iterator endpoint_iterator);

            void close();

        public:
            std::vector<char> buildHeader(const std::string& body);
            std::size_t parseHeader(const std::vector<char>& buffer);

        public:
            void sendMessage(const std::string &message);

            std::string readResponse();

        private:
            boost::asio::io_service &_ios;
            boost::asio::ip::tcp::socket sock;
            boost::system::error_code not_throw;

        private:
            enum {
                header_size = sizeof(std::size_t)
            };

        private:
            enum {
                max_length = 1024
            };
        };

    }
}

#endif //_TCPCLIENT_H
