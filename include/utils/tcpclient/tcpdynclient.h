
#pragma once
#ifndef __TCPDYNCLIENT_H__
#define __TCPDYNCLIENT_H__


// ------------------------------------
// Standard Library
// ------------------------------------
#include <string>
#include <array>
#include <functional>
#include <iostream>
#include <vector>

// ------------------------------------
// Boost
// ------------------------------------
#include <boost/asio.hpp>

using boost::asio::ip::tcp;

namespace utils {
    namespace tcp_utils {

        void noop();

        class TcpDynClient {
        public:
            explicit TcpDynClient(boost::asio::io_service &ios, boost::asio::ip::tcp::acceptor &acceptor);

            TcpDynClient() = delete;

            ~TcpDynClient();

        private:
            void connect(boost::asio::ip::tcp::acceptor &acceptor);
            void close();
            std::vector<char> buildHeader(const std::string& body);
            std::size_t parseHeader(const std::vector<char>& buffer);

        public:
            void sendMessage(const std::string &message);

            std::string readResponse();

        private:
            boost::asio::io_service &_ios;
            boost::asio::ip::tcp::socket sock;
            boost::system::error_code not_throw;

        private:
            enum {
                header_size = sizeof(std::size_t)
            };
        };

    }
}

#endif //__TCPDYNCLIENT_H__
