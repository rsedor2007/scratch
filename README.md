# Scratch

- [Overview](#Overview)
- [DevOps](#DevOps)
- [DevTools](#DevTools)
- [Libraries](#Libraries)

# Overview

This project is developed around the need to isolate and wrap ISD in a version independent manner from the core TLS code base.

## C++ Guidelines

[C++ Core](https://github.com/isocpp/CppCoreGuidelines/blob/master/CppCoreGuidelines.md) guidelines

[Stroustrup](http://www.stroustrup.com/bs_faq2.html) style and techniques FAQ

[Stroustrup](https://github.com/isocpp/CppCoreGuidelines/blob/master/CppCoreGuidelines.md#nl17-use-kr-derived-layout)
derived K&R style guide

# DevTools

- CLion
- CMake
- Boost 1.65fa
- GTest/GMock
- Protobuf

## CMake

[CLion Basics](https://www.jetbrains.com/help/clion/quick-cmake-tutorial.html#libs) using CMake.

GNUInstallDirs provides variables defined by the GNU Coding Standards.

[GNU Coding Standards](https://www.gnu.org/prep/standards/html_node/Directory-Variables.html)
[GNUInstallDirs](https://cmake.org/cmake/help/v3.12/module/GNUInstallDirs.html)

### CMAKE_CXX_FLAGS

CMake CXX Flags used are: `-std=c++11` for compatibility with the tool chain.

The C++ standard flags are:

````
-std=c++11  # C++ 11
-std=c++1y  # C++ 14
-std=c++1z  # C++ 17
````

PThread and Real Time Extensions may be required by some Boost libraries:

````
On Linux

-pthread    # PThread option
-lrt        # Real-time extensions
````

There may be a need to surround some code blocks with conditional compilation units.  Ex. when 
creating a daemon process.

````
To find Linux conditional defines from the command-line:

$ gcc -dM -E - < /dev/null | grep -i linux
#define __linux 1
#define __linux__ 1
#define __gnu_linux__ 1
#define linux 1
````
## Libraries

- Boost
- GTest
- Protobuf

### Boost

- Boost 

Boost will require linking non head-only libraries for; 

- thread
- asio
- ipc

````
target_link_libraries(${PROJECT_NAME}
        PUBLIC
        ${Boost_LIBRARIES}
        rt
        pthread
        )
````        

All three Boost libraries require `-pthread`.  

The Boost Interprocess library has a link to the real-time clock functionality.

It can be added to CMake as per the target link as per the above example or as part of the
CMAKE_CXX_FLAGS with `-lrt` 

or

````
find_library(LIBRT rt) 
if(LIBRT)
target_link_libraries(target_name ${LIBRT})
endif()
````

[LIBRT](https://www.unix.com/man-page/all/3LIB/librt/) man pages.

### GTest
Install the gtest development package on Ubuntu:

````
$ sudo apt install libgtest-dev
````

This will install all of the source files.  We have to compile the code and then move the compiled output to `/usr/lib`.

````
$ cd /usr/src/gtest
$ sudo cmake CMakeLists.txt
$ sudo make

# Copy or symlink libgtest.a and ligtests_main.a to our /usr/lib folder
$ sudo cp *a /usr/lib
````

GTest requires linking the GTest libraries and `pthread` linked in.

````
target_link_libraries(${PROJECT_NAME} ${GTEST_LIBRARIES} pthread)
````
#### Selectively running GTest

CMake allows us to selectively run tests.

`include(CTest)` or `enable_testing()` in our top level CMake file will add a `BUILD_TESTING` variable.
This defaults to **ON**.

Then we can selectively include our `Tests` subdirectories:

````
if (BUILD_TESTING)
    add_subdirectory(tests)
endif()
````
The CMake files in the subdirectories can then be used to add tests:

````
# Setup test dependencies
# setup google test
find_package(GTest REQUIRED)
include_directories(${GTEST_INCLUDE_DIRS})

add_executable(${PROJECT_NAME} 
    # ...
    ${SOURCES}
    ${HEADER_FILES}
    )
    
target_link_libraries(${PROJECT_NAME}
        PUBLIC
        ${GTEST_LIBRARIES}
        pthread
        )
        
add_test({unit_test_name}, ${PROJECT_NAME})
````

Selectively turning tests off at the command-line:

````
cmake -DBUILD_TESTS=OFF
````
If tests are turned on, the `-debug` option will be used to build the tests.  This will add a 
significant amount of overhead to a large build.

CMake then gives us the ability to run tests selectively through CTest:

- All tests
- Tests suites
- Single tests
- Run all tests and selectively turn off tests

[Testing with CTest](https://gitlab.kitware.com/cmake/community/wikis/doc/ctest/Testing-With-CTest)

# Sundries

## inotify

If you are on Ubuntu or Red Hat (AWS derived also) you may experience an `inotify` limit.  This is may
occur if you are using a Git tool such as `SourceTree` or `GitKraken` which watches files in repositories
for changes to occur.

In order to change this limit on Linux (Ubuntu or Red Hat) refer to the following:

[inotify](https://github.com/guard/listen/wiki/Increasing-the-amount-of-inotify-watchers) limit changes.

## Curl

[Curl](https://gist.github.com/caspyin/2288960) examples